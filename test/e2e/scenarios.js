'use strict';

/* http://docs.angularjs.org/guide/dev_guide.e2e-testing */

describe('my app', function() {

  beforeEach(function() {
	  localStorage.clear();
	  browser().navigateTo('../../app/index.html');
  });


  it('should automatically redirect to /view1 when location hash/fragment is empty', function() {
    expect(browser().location().url()).toBe("/measure");
  });


  describe('measure structure', function() {

    beforeEach(function() {
      browser().navigateTo('#/measure');
    });


    it('should render measure when user navigates to /measure', function() {
      expect(element('[ng-view] p:first').text()).
        toMatch(/Add a new value for your water meter!/);
      
      //TODO: add more tests for structure (e.g. input must be present, save button etc.)
    });
  });

  
  describe('measure value no water meter', function() {

	    beforeEach(function() {
	      browser().navigateTo('#/measure');
	    });

	    it('should show 00000.000 if no water meter exists in local storage', function() {
	    	if (localStorage.waterMeter == null) {
	    		expect(input("sensorValueText").val()).
	            toBe('00000.000');
			}
	      });
	  });
  
  describe('measure value water meter exists', function() {

	    beforeEach(function() {
	      
	    	browser().navigateTo('#/measure');
	    	
	    	/*
	    	 * Create a water meter locally.
	    	 */
	    	var waterMeter 				= { lastValue : "34.23" };
	      	localStorage.waterMeter 	= JSON.stringify(waterMeter);
	    });

	    it('should show localStorage.waterMeter.lastValue if a water meter exists in local storage', function() {
	    	
	    	if (localStorage.waterMeter != null) {
	    		
	    		expect(element("#sensorValueTextInput").val()).toBe("34.230");
			}
	      });
	  });

  describe('user clicks sensorValueText', function() {

	    beforeEach(function() {
	      browser().navigateTo('#/measure');
	    });

	    it('should be empty if it displays 00000.000' +
	 	   ' and the user clicks it', function() {
	    	
	    	input('sensorValueText').enter('00000.000');
	    	element("#sensorValueTextInput").click();
	    	expect(input("sensorValueText").val()).toBe('');
	      });
	  });
  
  describe('increment and decrement', function() {

	    beforeEach(function() {
	      browser().navigateTo('#/measure');
	    });

	    it('should add 0.001 to sensorValueText when the user presses +', function() {
	    	
	    	input('sensorValueText').enter('34.999');
	    	element("#incrementButton").click();
	    	expect(input("sensorValueText").val()).toBe('35.000');
	      });
	    
	    it('should substract 0.001 from sensorValueText when the user presses -', function() {
	    	
	    	input('sensorValueText').enter('35.911');
	    	element("#decrementButton").click();
	    	expect(input("sensorValueText").val()).toBe('35.910');
	      });
	  });
  
  	describe('calculate usage', function() {

	    beforeEach(function() {
	    	
	    	browser().navigateTo('#/measure');
	      
	      	/*
	    	 * Create a water meter locally.
	    	 */
	    	var waterMeter = { 
	    			lastValue    : 34.23,
	    			pricePerUnit : 2.304
	    	};
	      	localStorage.waterMeter 	= JSON.stringify(waterMeter);
	    });

	    it('display the correct usage after the user pressed Save', function() {
	    	
	    	input('sensorValueText').enter('36.230');
	    	element("#saveValueButton").click();
	    	expect(binding('usage')).toBe('2');
	    	
	    	expect(binding('priceForUsage')).toBe('4.608');
	      });
	  });
  	
  	describe('Handle input less than last value', function() {

	    beforeEach(function() {
	    	
	    	browser().navigateTo('#/measure');
	      
	      	/*
	    	 * Create a water meter locally.
	    	 */
	    	var waterMeter 				= { lastValue : "34.23" };
	      	localStorage.waterMeter 	= JSON.stringify(waterMeter);
	    });

	    it('it should display the last value if the new value is less than the last', function() {
	    	
	    	input('sensorValueText').enter('34.100');
	    	element("#saveValueButton").click();
	    	expect(input("sensorValueText").val()).toBe('34.230');
	      });
	  });
  	
  	describe('Settings', function() {

	    beforeEach(function() {
	    	
	    	browser().navigateTo('#/settings');
	      
	      	/*
	    	 * Create a water meter locally.
	    	 */
	    	var waterMeter = { 
	    			lastValue    : "34.23",
	    			pricePerUnit : 2.034
	    	};
	      	localStorage.waterMeter 	= JSON.stringify(waterMeter);
	    });

	    it('it should display pricePerUnit if a waterMeter exists in localStorage', function() {
	    	
	    	expect(input("unitPrice").val()).toBe('2.034');
	      });
	  });
});
