'use strict';

/* jasmine specs for controllers go here */

describe('SensorController', function(){
  
	beforeEach(module('myApp.services'));

  afterEach(function(){
	 localStorage.clear();
  });
  
  
  it('should create a water meter locally' + 
     ' if no water meter exists in local storage' + 
     ' and user saves a new value', function() {
    
	  /*
	   * Mock the $resource and settingsService that 
	   * are injected into SensorController.
	   */
	  module(function($provide) {
			$provide.factory('$resource', function() {
				return jasmine.createSpy();
			});
			$provide.factory('settingsService', function() {
				return {pricePerUnit : jasmine.createSpy()};
			})
	  });
	  
	  inject(function($injector, $controller) {
		  
		  var scope = {};
		  scope.sensorValueText = '13.2';
		  
		  /*
		   * Get the SettingsController injected with
		   * a settingsSerive instance.
		   */
		  var ctrl = $controller(SensorController, {$scope: scope});
		  
		  ctrl.addNew();
	  
		  expect(localStorage.waterMeter).toBeDefined(); 
	  
		  var waterMeter = JSON.parse(localStorage.waterMeter);
	  
		  expect(waterMeter.lastValue).toBe(13.2)
	  })
  });
  
  it('should pad a sensor value text with zeros', function() {
	
	  var scope = {};
	  
	  var ctrl = new SensorController(scope, null, null);
	  
	  var padResult = ctrl.paddWithZeros('13');
	  expect(padResult).toBe('13.000');
	  
	  var padResult = ctrl.paddWithZeros('13.1');
	  expect(padResult).toBe('13.100');
	  
	  var padResult = ctrl.paddWithZeros('13.13');
	  expect(padResult).toBe('13.130');
	  
	  var padResult = ctrl.paddWithZeros('13.133');
	  expect(padResult).toBe('13.133');
  });
  
  it('should calculate the usage', function() {
	  
	  var scope = {};
	  
	  var ctrl = new SensorController(scope, null, null);
	  
	  var usage = ctrl.calculateUsage(123.43, 123.45);
	  expect(usage).toBe(0.02);
	  
	  usage = ctrl.calculateUsage(123.432, 123.435);
	  expect(usage).toBe(0.003);
  });
  
  it('should calculate the price of the usage', function() {
	  
	  /*
	   * Mock the $resource that is injected into
	   * SensorController.
	   */
	  module(function($provide) {
			$provide.factory('$resource', function() {
				return jasmine.createSpy();
			})
	  });
	  
	  inject(function($injector, $controller) {
	  
		  var settingsService = $injector.get('settingsService');
		  var settings = {
   			   pricePerUnit : 2.035
   	   	  };
   	   	  settingsService.saveSettings(settings);
   	   	  
		  /*
		   * Get the SettingsController injected with
		   * a settingsSerive instance.
		   */
		  var ctrl = $controller(SensorController, {$scope: {}});
		  
		  var price = ctrl.priceForUsage(1);
		  
		  expect(price).toBe(2.035);
	  
	  })
  });
});


describe('SettingsController', function(){
	
	  it('should call settingsService.saveSettings once' + 
	     'upon call to saveSettings', function() {
		  
		  /*
		   * Mock the settingsService that is injected into
		   * SettingsController.
		   */
		  module(function($provide) {
				$provide.factory('settingsService', function() {
					return {saveSettings: jasmine.createSpy()};
				})
		  });
		  
		  inject(function($injector, $controller) {
			  
			  /*
			   * Get the SettingsController injected with
			   * a settingsSerive instance (the mock in this case).
			   */
			  var ctrl = $controller(SettingsController, {$scope: {}});
			  
			  ctrl.saveSettings();
			  
			  var settingsService = $injector.get('settingsService');
			  expect(settingsService.saveSettings.callCount).toEqual(1);
		  });
	  });
	});