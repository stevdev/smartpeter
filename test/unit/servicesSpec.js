'use strict';

angular.module('myAppTestModule', ['myApp.services']);

/* jasmine specs for services go here */

describe('services', function() {
  
	  beforeEach(module('myApp.services'));
	  
	  afterEach(function(){
		  localStorage.clear();
	  });
	  
	  describe('version', function() {
	    it('should return current version', inject(function(version) {
	      expect(version).toEqual('0.1');
	    }));
	  });
	  
	  describe('settingsService', function() {
		    
		  it('should create a water meter in localStorage ' + 
		       'if none exists before a call to saveSettings ' +
		       'and update it in successive calls', 
		       
		       inject(function(settingsService) {
		    	   
		    	   expect(settingsService).toBeDefined();
		    	   
		    	   localStorage.clear();
		    	   
		    	   var settings = {
		    			   pricePerUnit : 2.035
		    	   };
		    	   
		    	   settingsService.saveSettings(settings);
		    	 
		    	   var waterMeter = JSON.parse(localStorage.waterMeter);
		    	   
		    	   expect(waterMeter.pricePerUnit).toBe(2.035);
		    	   
		    	   //do another call
		    	   
		    	   var settings = {
		    			   pricePerUnit : 2.18
		    	   };
		    	   
		    	   settingsService.saveSettings(settings);
		    	   
		    	   waterMeter = JSON.parse(localStorage.waterMeter);
		    	   
		    	   expect(waterMeter.pricePerUnit).toBe(2.18);
		    	   
		    	   expect(settingsService.pricePerUnit()).toBe(2.18);
		    }));
		  
		  it('should convert a pricePerUnit as string to a number', 
			       
				  inject(function(settingsService) {
			    	   
			    	   expect(settingsService).toBeDefined();
			    	   
			    	   localStorage.clear();
			    	   
			    	   var settings = {
			    			   pricePerUnit : "2.035"
			    	   };
			    	   
			    	   settingsService.saveSettings(settings);
			    	 
			    	   var waterMeter = JSON.parse(localStorage.waterMeter);
			    	   
			    	   expect(waterMeter.pricePerUnit).toBe(2.035);
			    }));
	  });
});
