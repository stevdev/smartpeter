'use strict';

/* Controllers */


function SensorController($scope, $http, $resource, settingsService) {
	
	var EMPTY_SENSOR_VALUE_INPUT = '00000.000'; 
	
	if ($scope.sensorValueText == null) {
		$scope.sensorValueText = EMPTY_SENSOR_VALUE_INPUT;
	}
	
	var waterMeter = null;
	
	if (localStorage.waterMeter != undefined 
		&& 
		localStorage.waterMeter != null
		&&
		localStorage.waterMeter != "") {
		
		waterMeter         	   = JSON.parse(localStorage.waterMeter);
		
		if (waterMeter.lastValue != undefined) {
			$scope.sensorValueText = _paddWithZeros(waterMeter.lastValue.toString());
		}
	}
	
	/*
	 * Why do we have a function definition and an assignment a variable?
	 * 
	 * Because we want to do two things:
	 * 1) call a function before it is declared.
	 * 2) call a function in a unit test.
	 * 
	 * For 1) we need a named function definition 
	 * ('this.func = function () {}' is not sufficient).
	 * 
	 * For 2) we need 'this.funcName = _funcName'
	 */
	this.paddWithZeros = _paddWithZeros;
	
	function _paddWithZeros(sensorValueText) {
		
		var result = ''
			
		var delimiterIndex = sensorValueText.indexOf('.');
		
		if (delimiterIndex == -1) {
			
			result = sensorValueText + '.000';
			
		} else {
			
			var subStrLength = sensorValueText.substring(delimiterIndex+1, sensorValueText.length).length; 
			
			switch (subStrLength) {
				case 0:  result = sensorValueText + '.000'; break;
				case 1:  result = sensorValueText + '00'; break;
				case 2:  result = sensorValueText + '0'; break;
				default: result = sensorValueText;
			}
		}
		
		return result;
	}
	
	this.calculateUsage = _calculateUsage;
	
	function _calculateUsage(currentMeasuredValue, lastMeasuredValue) {
		return  Math.round(((lastMeasuredValue - currentMeasuredValue))*1000)/1000;
	}
	
	this.priceForUsage = _priceForUsage;
	function _priceForUsage(usage) {
		return  Math.round(((usage * settingsService.pricePerUnit()))*1000000)/1000000;
	}
	
	this.addNew = function() {
		
		if (waterMeter == null) {
			waterMeter = { lastValue : 0 };
		}
		
		var newValue = parseFloat($scope.sensorValueText);
		
		if (newValue > waterMeter.lastValue) {
			
			$scope.usage         = _calculateUsage(waterMeter.lastValue, newValue);
			$scope.priceForUsage = _priceForUsage($scope.usage);
			
			waterMeter.lastValue = newValue;
			
			/*
			 * Update localStorage.
			 */
			localStorage.waterMeter = JSON.stringify(waterMeter);
		} else {
			
			if (waterMeter.lastValue != undefined) {
				$scope.sensorValueText = _paddWithZeros(waterMeter.lastValue.toString());
			}
		}
		
		
//		/*
//		 * Without deleting X-Requested-With the request did not succeed.
//		 */
//		delete $http.defaults.headers.common['X-Requested-With'];
//		
//		var SensorLog = $resource('https://api.mongohq.com/databases/:db/collections/:col/documents',
//								  {db:"dieterkrebs", col:"SensorLog", _apikey:"eu2roc9lb2ofr5un4n86"});
//		
//		var newLog      = new SensorLog();
//		newLog.document = { 
//							sensorValue : $scope.sensorValue,
//				            takenOn     : new Date().getTime()
//				          };
//		newLog.$save();

	};
	
	$scope.addNew = this.addNew;
	
	$scope.sensorValueInputSelected = function() {
		
		if ($scope.sensorValueText == EMPTY_SENSOR_VALUE_INPUT) {
			$scope.sensorValueText = '';
		}
	}
	
	$scope.increment = function() {
		
		var value = parseFloat($scope.sensorValueText);
		//round value to three decimals (http://www.javascriptkit.com/javatutors/round.shtml)
		value = Math.round((value + 0.001)*1000)/1000;
		$scope.sensorValueText = _paddWithZeros(value.toString());
	}
	
	$scope.decrement = function() {
		
		var value = parseFloat($scope.sensorValueText);
		//round value to three decimals (http://www.javascriptkit.com/javatutors/round.shtml)
		value = Math.round((value - 0.001)*1000)/1000;
		
		if (value <= 0) {
			$scope.sensorValueText = '00000.000';
		} else {
			$scope.sensorValueText = _paddWithZeros(value.toString());
		}
	}
}

SensorController.$inject = ['$scope','$http', '$resource', 'settingsService'];

function SettingsController($scope, settingsService) {
	
	if (localStorage.waterMeter != undefined 
		&& 
		localStorage.waterMeter != null) 
	{		
		var waterMeter = JSON.parse(localStorage.waterMeter);
			
		if(waterMeter.pricePerUnit != undefined
		   &&
		   waterMeter.pricePerUnit != null) 
		{
			$scope.unitPrice = waterMeter.pricePerUnit;
		}
	}
	
	function _saveSettings(){
		
		var settings = {
				pricePerUnit : $scope.unitPrice
		};
		settingsService.saveSettings(settings);
		
		$scope.statusMessage = 'Settings saved.';
	}
	
	this.saveSettings   = _saveSettings;
	$scope.saveSettings = _saveSettings; 
}

SettingsController.$inject = ['$scope','settingsService'];

