'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', ['ngResource', 
                         'myApp.filters', 
                         'myApp.services', 
                         'myApp.directives']
).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/measure', {templateUrl: 'partials/measure.html', controller: SensorController});
    $routeProvider.when('/settings', {templateUrl: 'partials/settings.html', controller: SettingsController});
    $routeProvider.otherwise({redirectTo: '/measure'});
  }]);
