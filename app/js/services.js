'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('myApp.services', [], function($provide) {
	
	$provide.factory('settingsService', function() {
	    var settingsService = {};
	    
	    settingsService.saveSettings = function (settings) {
	    	
	    	var waterMeter = localStorage.waterMeter;
	    	
	    	if(waterMeter == null) {
	    		waterMeter = { 
	    				pricePerUnit : 0		
	    		};
	    	} else {
	    		waterMeter = JSON.parse(localStorage.waterMeter);
	    	}
	    	
	    	var price = settings.pricePerUnit;
	    	
	    	if (price.substring) { //is it a string?
	    		price = parseFloat(price);
	    	}
	    	
	    	waterMeter.pricePerUnit = price;
	    	
	    	/*
			 * Update localStorage.
			 */
			localStorage.waterMeter = JSON.stringify(waterMeter);
	    };
	    
	    settingsService.pricePerUnit = function () {
	    	
	    	var price = 1;
	    	
	    	var waterMeter = localStorage.waterMeter;
	    	
	    	if(waterMeter != null) {
	    		waterMeter = JSON.parse(localStorage.waterMeter);
	    		price = waterMeter.pricePerUnit;
	    	} 
	    	
	    	return price;
	    };
	    
	    return settingsService;
	  });
	}).
	
  value('version', '0.1');
